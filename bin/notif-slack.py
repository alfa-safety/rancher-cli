from slacker import Slacker

import sys
import os
import getopt
from optparse import OptionParser

def getOptions(Args):
  parser = OptionParser()
  parser.add_option("-m", "--message", action="store", type="string", help="Message")
  parser.add_option("-c", "--channel", action="store", type="string", help="Channel to post")
  parser.add_option("-t", "--token", action="store", type="string", help="Token to use")
  parser.add_option("-u", "--username", action="store", type="string", help="Slack username")

  (options, args) = parser.parse_args()

  return options


listArgs=sys.argv
#print(listArgs)
info = getOptions(listArgs)

slack = Slacker(info.token)
channel = info.channel
message = info.message
user = info.username

slack.chat.post_message(channel, message, user, icon_url='https://alfasafety-public.s3-eu-west-1.amazonaws.com/claptrap.jpg')
