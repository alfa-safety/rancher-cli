import sys
import getopt
import re
from optparse import OptionParser
import json
import os
import subprocess
import time

def getOptions(Args):
  parser = OptionParser()
  parser.add_option("-n", "--namespace", action="store", type="string", help="Namespace to rollout")
  parser.add_option("-f", "--filter", action="store", type="string", help="string in name to filter")

  (options, args) = parser.parse_args()

  return options


listArgs=sys.argv
#print(listArgs)
info = getOptions(listArgs)
list_all = os.popen("rancher kubectl get deployments -n " + info.namespace + " |grep '"+ info.filter +"'|awk '{print $1}'").readlines()
for deployment in list_all:
    print(deployment)
    cmd = "rancher kubectl rollout restart -n " + info.namespace + " deployment/" + deployment
    print(cmd)
    redeploy = os.popen(cmd).read()
    print(redeploy)
