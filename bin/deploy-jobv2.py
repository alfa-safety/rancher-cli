import sys
import getopt
import re
from optparse import OptionParser
import json
import os
import subprocess
import time

def getOptions(Args):
  parser = OptionParser()
  parser.add_option("-a", "--app", action="store", type="string", help="Application to upgrade")
  parser.add_option("-n", "--namespace", action="store", type="string", help="Namespace to deploy in")
  parser.add_option("-t", "--template", action="store", type="string", help="App template")
  parser.add_option("-i", "--image", action="store", type="string", help="Image Tag to deploy")
  parser.add_option("-x", "--tag", action="store", type="string", help="Image Tag to deploy")
  parser.add_option("-r", "--registry", action="store", type="string", help="Registry Secrets")
  parser.add_option("-b", "--rancher", action="store", type="string", help="Rancher binary path", default="/usr/bin/rancher")

  (options, args) = parser.parse_args()

  return options


listArgs=sys.argv
#print(listArgs)
info = getOptions(listArgs)
print(info.app)
list_app = os.popen(info.rancher + " apps ls |grep " + info.app).read()
if (info.app in list_app):
    delete_job = subprocess.Popen(info.rancher + " apps delete " + info.app, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = delete_job.communicate()
    print(output)
    print(error)
    while (info.app in list_app):
      time.sleep(2)
      list_app = os.popen(info.rancher + " apps ls |grep " + info.app).read()
    print("previous DB Upgrade workload has been successfully removed")
      
cpt = 0
cmd = info.rancher + " apps install --set image.repository=" + info.image + " --set image.tag=" + info.tag + " --set image.registry=" + info.registry +" --namespace " + info.namespace + " --helm-timeout 600" + " " + info.template + " " + info.app + " --version 1"
print(cmd)
install_job = subprocess.Popen(cmd + " " + info.app, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
output, error = install_job.communicate()
print(output)
print(error)
print("New DB Upgrade workload has been successfully launched")
