import sys
import getopt
import re
from optparse import OptionParser
import json
import os
import subprocess
import time

def getOptions(Args):
  parser = OptionParser()
  parser.add_option("-a", "--app", action="store", type="string", help="Application to upgrade")
  parser.add_option("-i", "--image", action="store", type="string", help="Image to deploy")
  parser.add_option("-t", "--tag", action="store", type="string", help="Tag to deploy")
  parser.add_option("-b", "--rancher", action="store", type="string", help="Rancher binary path", default="/usr/bin/rancher")

  (options, args) = parser.parse_args()

  return options


listArgs=sys.argv
#print(listArgs)
info = getOptions(listArgs)
print(info.app)
current_chart = os.popen(info.rancher + " app rollback " + info.app + " -r |grep '*'|sed 's/   */;/g'").read()
current_chart = current_chart.split(';')
current_version = current_chart[4]
old_revision = current_chart[1]

deployment = subprocess.Popen(info.rancher + " app upgrade " + info.app + " --set image.repository=" + info.image +" --set image.tag=" + info.tag + " " + current_version, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
output, error = deployment.communicate()

cpt=0
while cpt < 1800:
  check = os.popen(info.rancher + " app ls |grep " + info.app + "|sed 's/   */;/g'").read()
  check = check.split(';')
  status = check[2]
  if ("active" in status):
    print("New revision has been successfully deployed")
    result = "Info on " + info.app + ": the image " + info.tag + " has been successfully deployed."
    subprocess.Popen("python /rancher/notif-slack.py -c " + os.environ['SLACK_CHANNEL'] + " -m '" + result + "' -t " + os.environ['SLACK_TOKEN'] + " -u "+ os.environ['SLACK_USER'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    cpt = 1800
  if ("failed" in status):
    print("Error : The deployment is failing")
    print("Rollback to previous image")
    result = "Error on " + info.app + " : Deployment of the image " + info.tag + " has failed. We are rolling back to the previous image"
    subprocess.Popen("python /rancher/notif-slack.py -c " + os.environ['SLACK_CHANNEL'] + " -m " + result + " -t " + os.environ['SLACK_TOKEN'] + " -u "+ os.environ['SLACK_USER'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    subprocess.Popen(info.rancher + " app rollback " + info.app + " " + old_revision, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True) 
    os.popen("exit 1")
  print("Processing Upgrade on " + info.app)
  time.sleep(2)
  cpt = cpt + 2
  if (cpt == 1800):
    print("Deployment is failing and has timed out")
    print("Rollback to previous image")
    result = "Error on "+ info.app + ": Deployment of the image " + info.tag + " has timed out. We are rolling back to the previous image"
    subprocess.Popen("python /rancher/notif-slack.py -c " + os.environ['SLACK_CHANNEL'] + " -m " + result + " -t " + os.environ['SLACK_TOKEN'] + " -u "+ os.environ['SLACK_USER'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    subprocess.Popen(info.rancher + " app rollback " + info.app + " " + old_revision, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
