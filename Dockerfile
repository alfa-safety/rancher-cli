FROM ubuntu:latest

RUN mkdir /rancher
WORKDIR /rancher
ADD ./bin /rancher
RUN apt-get update -y
RUN apt-get install -y python3 python3-pip
RUN cp ./rancher /usr/bin/
RUN cp ./kubectl /usr/bin/
RUN pip install slacker
ENV RANCHER_SERVER_URL=""
ENV RANCHER_CI_TOKEN=""
ENV RANCHER_PROJECT_ID=""
